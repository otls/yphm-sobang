<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yphm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'BmZqhSMrZOpjUGPjKakaUw7AeNXgjyYqfqnVskavWyaD5JscMrCPFeSBB6qBHsol' );
define( 'SECURE_AUTH_KEY',  'ZUlTFVTyaqRflVh5ytrlGHx21HtYsT1KBicNcBKF9DoxguNUdDoBqUheQX1N4IJd' );
define( 'LOGGED_IN_KEY',    'gpKU6kvju4AjPvyZAyaAlhnuVOFrJ0GGvbSB81zCfdotSusrP8tKhTBrvGTlnEx5' );
define( 'NONCE_KEY',        'Doa8HFEHXFtbCKwMYQyVruZxmlcDn1mODwbXdeYVnRW7JMKkg0ukSYYIUIDfLLwP' );
define( 'AUTH_SALT',        'rZmjU5B2bsSoc4MiFQHsAVQy8MEVmGn6yEkyQA5VxvNJqMiSO4Bye8ZXKO4dK1Tn' );
define( 'SECURE_AUTH_SALT', 'zTZd7WYsWOcytym4RTnFc7z0yAU2Sp5BBEviqm79IIR6hqCDKtbxHTUJ3dF7Ct0d' );
define( 'LOGGED_IN_SALT',   'afJbgw13u1pEikrbP4lr1n9zurTNbaM1QVKMWc30ViJ1ny6cKCvDQ79P18zPMMbt' );
define( 'NONCE_SALT',       'TUPq3GotgPb9dZsNAQwGoa4q6QZSjK0iJ9Ok3OKE0jGc0J3euapDDM8lXJXIFZMe' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
